<?php

use Illuminate\Database\Seeder;

class TransportationTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\TransportationType::create([
          'description'       => 'Kereta Api'
        ]);

        App\TransportationType::create([
          'description'       => 'Pesawat'
        ]);
    }
}
