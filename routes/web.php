<?php
Auth::routes();
Route::get('/', function () {
	return view('welcome');
});

Route::prefix('admin')->namespace('Admin')->group(function(){
	//Transportasi
	Route::get('/transportasi', 'TransportasiController@index');
	Route::get('/transportasi/add', 'TransportasiController@add');
	Route::get('/transportasi/{transportation}', 'TransportasiController@show');
	Route::get('/transportasi/{transportation}/edit', 'TransportasiController@edit');
	Route::post('/transportasi', 'TransportasiController@store');
	Route::patch('/transportasi', 'TransportasiController@update');
	Route::delete('/transportasi', 'TransportasiController@delete');

	Route::get('/terminal', 'TerminalController@index');
	Route::post('/terminal', 'TerminalController@store');
	Route::get('/terminal/{terminal}/edit', 'TerminalController@edit');
	Route::patch('/terminal', 'TerminalController@update');
	Route::delete('/terminal', 'TerminalController@delete');
	
	Route::get('rute', 'RuteController@index');
	Route::get('rute/add', 'RuteController@add');
	Route::get('rute/{rute}/edit', 'RuteController@edit');
	Route::post('rute', 'RuteController@store');
	Route::patch('rute', 'RuteController@update');
	Route::delete('rute', 'RuteController@delete');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/train', 'KeretaController@index')->name('kereta');
Route::get('/train/search', 'KeretaController@search');
Route::get('/plane', 'PesawatController@index')->name('pesawat');
Route::get('/plane/search', 'PesawatController@search');

Route::get('booking', 'BookingController@index');
Route::post('booking', 'BookingController@index');
Route::post('booking/seat', 'BookingController@seat');
Route::post('booking/confirm-booking', 'BookingController@confirm');
Route::post('booking/make-booking', 'BookingController@make');
