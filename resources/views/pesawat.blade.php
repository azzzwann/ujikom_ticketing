@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header"><i class="fa fa-plane"></i>Pesawat</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="form-search" action="{{url('plane/search')}}" method="get">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group {{ $errors->has('price') ? ' has-error has-danger' : '' }}">
                            <label class="col-form-label">Tanggal</label>

                            <div class="row">
                              <div class="col-md-12">
                                  <input type="date" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" value="{{ count($errors) ? old('date') : date('Y-m-d') }}" required>

                                  @if ($errors->has('date'))
                                      <span class="invalid-feedback">
                                          <strong>{{ $errors->first('date') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group {{ $errors->has('rute_from') ? ' has-error has-danger' : '' }}">
                        <label class="col-form-label">Bandara Asal</label>

                        <div class="row">
                          <div class="col-md-6">
                            <select class="form-control" name="rute_from">
                              @foreach ($terminals as $t)
                                <option value="{{$t->id}}">{{$t->name}}</option>
                              @endforeach
                            </select>

                              @if ($errors->has('rute_from'))
                                  <span class="invalid-feedback">
                                      <strong>{{ $errors->first('rute_from') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('rute_to') ? ' has-error has-danger' : '' }}">
                        <label class="col-form-label">Bandara Tujuan</label>

                        <div class="row">
                          <div class="col-md-6">
                            <select class="form-control" name="rute_to">
                              @foreach ($terminals as $t)
                                <option value="{{$t->id}}">{{$t->name}}</option>
                              @endforeach
                            </select>

                              @if ($errors->has('rute_to'))
                                  <span class="invalid-feedback">
                                      <strong>{{ $errors->first('rute_to') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                    </div>

                    

                    <div class="form-group {{ $errors->has('adult') ? ' has-error has-danger' : '' }}">
                        <label class="col-form-label">Jumlah Dewasa</label>

                        <div class="row">
                          <div class="col-md-6">
                            <select class="form-control" name="adult">
                              @for ($i=1; $i <= 4; $i++)
                                <option>{{$i}}</option>
                              @endfor
                            </select>

                              @if ($errors->has('adult'))
                                  <span class="invalid-feedback">
                                      <strong>{{ $errors->first('adult') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('infant') ? ' has-error has-danger' : '' }}">
                        <label class="col-form-label">Jumlah Infant</label>

                        <div class="row">
                          <div class="col-md-6">
                            <select class="form-control" name="infant">
                              @for ($i=0; $i <= 4; $i++)
                                <option>{{$i}}</option>
                              @endfor
                            </select>

                              @if ($errors->has('infant'))
                                  <span class="invalid-feedback">
                                      <strong>{{ $errors->first('infant') }}</strong>
                                  </span>
                              @endif
                          </div>
                        </div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary" onclick="event.preventDefault();
                                  document.getElementById('form-search').submit();">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
