@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header"><i class="fa fa-train"></i> Kereta Api</div>

                <div class="card-body">
                  <form action="{{url('booking/seat')}}" method="post">
                    <input type="hidden" name="date" value="{{app('request')->get('date')}}">
                    <input type="hidden" name="rute" value="{{app('request')->get('rute')}}">
                    {{ csrf_field() }}
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                  <h4 style="text-align: center;">Data Booking</h4>
                  <span style="font-size: 10pt">Data Penumpang</span>
                  @for ($i=0; $i < app('request')->adult; $i++)
                    <div class="col-md-12" style="margin-top: 10px;padding: 0px">
                      <div class="row">
                        <div class="col-md-6" style="padding: 0px">
                            <div class="col-md-3">
                                <span style="font-size: 10pt">Nama</span>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="name[{{$i}}]" class="form-control" placeholder="Nama Penumpang" required>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 0px">
                            <div class="col-md-3">
                                <span style="font-size: 10pt">Jenis Kelamin</span>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" name="gender[{{$i}}]">
                                  <option>Laki-laki</option>
                                  <option>Perempuan</option>
                                </select>
                            </div>
                        </div>
                      </div>
                    </div>
                  @endfor

                  {{-- @for ($i=0; $i < app('request')->infant; $i++)
                    <div class="col-md-12" style="margin-top: 10px;padding: 0px">
                      <div class="row">
                        <div class="col-md-6" style="padding: 0px">
                            <div class="col-md-3">
                                <span style="font-size: 10pt">Nama</span>
                            </div>
                            <div class="col-md-9">
                                <input type="text" name="name_infant[{{$i}}]" class="form-control" placeholder="Nama Penumpang" required>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding: 0px">
                            <div class="col-md-3">
                                <span style="font-size: 10pt">Jenis Kelamin</span>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" name="gender_infant[{{$i}}]">
                                  <option>Laki-laki</option>
                                  <option>Perempuan</option>
                                </select>
                            </div>
                        </div>
                      </div>
                    </div>
                  @endfor --}}

                  <hr style="margin-top: 20px !important ; width: 100%; border: 0; height: 1px; background-color:  #D0D9D6;">
                  <br>
                  <span style="font-size: 10pt;">Data Pemesan</span>
                  <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                  <div class="col-md-12" style="margin-top: 10px;padding: 0px">
                      <div class="col-md-6" style="padding: 0px">
                          <div class="col-md-3">
                              <span style="font-size: 10pt">Nama</span>
                          </div>
                          <div class="col-md-9">
                              <input type="text" class="form-control" placeholder="Nama pemesan" value="{{Auth::user()->fullname}}" readonly>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-12" style="margin-top: 20px; padding: 0px">
                      <div class="col-md-6" style="padding: 0px">
                          <div class="col-md-3">
                              <span style="font-size: 10pt">No. HP</span>
                          </div>
                          <div class="col-md-9">
                              <input type="text" name="phone" class="form-control" placeholder="No.HP" required>
                              <span style="font-size: 8pt">*Nomor yang dapat dihubungi</span>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-12" style="margin-top: 20px; padding: 0px">
                      <div class="col-md-6" style="padding: 0px">
                          <div class="col-md-3">
                              <span style="font-size: 10pt">Alamat</span>
                          </div>
                          <div class="col-md-9">
                              <textarea class="form-control" name="address" placeholder="Alamat" required></textarea>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-2" style="float: right; margin-top: 20px; margin-bottom: 20px">
                      <button type="submit" class="btn btn-primary" style="float: right; width: 100%">Submit</button>
                  </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
