<ul class="sidebar-menu" data-widget="tree">
  <li class=""><a href="{{ url('admin/transportasi') }}"><i class="fa fa-train"></i> <span>Transportasi</span></a></li>
  <li class=""><a href="{{ url('admin/terminal') }}"><i class="fa fa-tag"></i> <span>Terminal</span></a></li>
  <li class=""><a href="{{ url('admin/rute') }}"><i class="fa fa-road"></i> <span>Rute</span></a></li>
</ul>