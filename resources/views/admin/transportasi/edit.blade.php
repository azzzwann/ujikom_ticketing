@extends('layouts.admin-layout')

@section('title')
  <i class="fa fa-train"></i> Edit Transportasi
@endsection

@section('content')
  <form action="{{url('admin/transportasi')}}" method="post">
    {{ csrf_field() }}
    {{ method_field('patch') }}
    <input type="hidden" name="transportation_id" value="{{$transportation->id}}">
    <div class="form-group {{ $errors->has('transportation_type') ? ' has-error has-danger' : '' }}">
      <label class="label">Tipe Transportasi</label>
      <div class="row">
        <div class="col-md-5">
          <select class="form-control" name="transportation_type">
            @foreach ($transportation_types as $tp)
              @if ($tp->id == $transportation->transportation_type_id)
                <option value="{{$tp->id}}" selected>{{$tp->description}}</option>
              @else
                <option value="{{$tp->id}}">{{$tp->description}}</option>
              @endif
            @endforeach
            @if ($errors->has('name'))
              <span class="invalid-feedback">
                  <strong>{{ $errors->first('transportation_type') }}</strong>
              </span>
            @endif
          </select>
        </div>
      </div>
    </div>
    <div class="form-group {{ $errors->has('code') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Kode Transportasi</label>

        <div class="row">
          <div class="col-md-6">
              <input type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ $transportation->code }}" placeholder="Kode Transportasi" required readonly>

              @if ($errors->has('code'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('code') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('description') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Nama Transportasi</label>

        <div class="row">
          <div class="col-md-6">
              <input type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ count($errors) ? old('description') : $transportation->description }}" placeholder="Nama Transportasi" required>

              @if ($errors->has('description'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('description') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>
    <hr>
    <button type="save" class="btn btn-success">Simpan</button>
  </form>
@endsection
