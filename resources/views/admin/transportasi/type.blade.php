@extends('layouts.admin-layout')

@section('title')
  <i class="fa fa-tag"></i> Tipe Transportasi
@endsection

@section('content')
{{-- Modal Insert --}}
<div class="modal fade" id="insertModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Tipe Transportasi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="add-form" action="{{url('admin\transportation_type')}}" method="post">
          {{ csrf_field() }}
          <div class="form-group {{ $errors->has('name') ? ' has-error has-danger' : '' }}">
							<label class="form-control-label">
								Nama Tipe Transportasi
							</label>
							<input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="Nama Tipe Transportasi" required>
							@if ($errors->has('name'))
                 <div class="form-control-feedback">
                   <strong>{{ $errors->first('name') }}</strong>
                 </div>
	            @endif
						</div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary" onclick="event.preventDefault();
                        document.getElementById('add-form').submit();">Simpan</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Insert --}}

{{-- Modal Edit --}}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Tipe Transportasi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="edit-form" action="{{url('admin\transportation_type')}}" method="post">
          {{ csrf_field() }}
          {{ method_field('patch') }}
          <input type="hidden" name="transportation_type_id">
          <div class="form-group {{ $errors->has('edit_name') ? ' has-error has-danger' : '' }}">
							<label class="form-control-label">
								Nama Tipe Transportasi
							</label>
							<input type="text" name="edit_name" class="form-control" value="{{old('edit_name')}}" placeholder="Nama Tipe Transportasi" required>
							@if ($errors->has('edit_name'))
                 <div class="form-control-feedback">
                   <strong>{{ $errors->first('edit_name') }}</strong>
                 </div>
	            @endif
						</div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary" onclick="event.preventDefault();
                        document.getElementById('edit-form').submit();">Edit</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Edit --}}

{{-- Modal Delete --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Tipe Transportasi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus tipe transportasi ini?
      </div>
      <div class="modal-footer">
        <form id="delete-form" action="{{url('admin\transportation_type')}}" method="post">
          {{ csrf_field() }}
          {{ method_field('delete') }}
          <input type="hidden" name="transportation_type_id">
          <button type="submit" class="btn btn-danger" onclick="event.preventDefault();
                      document.getElementById('delete-form').submit();">Hapus</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Delete --}}

@if (session('status'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times</button>
        <strong>Selamat</strong>
        {{ session('status') }}
    </div>
@endif
<div class="row">
  <div class="col-md-12">
    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#insertModal">
      <i class="fa fa-plus"></i> Tambah Tipe Transportasi
    </button>
  </div>
</div>
<hr>
<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th>Nama Tipe Transportasi</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @php($salim = 1)
    @foreach ($transportation_types as $tp)
      <tr>
        <td>{{$salim++}}</td>
        <td>{{$tp->description}}</td>
        <td>
          <button class="btn btn-success" data-toggle="modal" data-target="#editModal" onclick="editModal({{$tp->id}},'{{$tp->description}}')"><i class="fa fa-edit"></i></button>
          <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="deleteModal({{$tp->id}},'{{$tp->description}}')"><i class="fa fa-close"></i></button>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
@endsection

@section('js')
  <script type="text/javascript">
    function editModal(id, name) {
      $('#editModal [name=transportation_type_id]').val(id);
      $('#editModal [name=edit_name]').val(name);
    }

    function deleteModal(id, name) {
      $('#deleteModal .modal-body').html('Apakah Anda yakin ingin menghapus tipe transportasi: '+name+'?');
      $('#deleteModal [name=transportation_type_id]').val(id);
    }
  </script>
@endsection
