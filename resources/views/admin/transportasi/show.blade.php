@extends('layouts.admin-layout')

@section('title')
  <i class="fa fa-train"></i> Kursi di Transportasi
@endsection

@section('content')
{{-- Modal Insert --}}
<div class="modal fade" id="insertModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Kursi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="add-form" action="{{url('admin\seat')}}" method="post">
          @csrf
          <input type="hidden" name="transportation_id" value="{{$transportation->id}}">
          <div class="form-group {{ $errors->has('seat_quantity') ? ' has-error has-danger' : '' }}">
							<label class="form-control-label">
								Berapa jumlah kursi yang ingin Anda tambahkan?
							</label>
							<input type="number" name="seat_quantity" class="form-control" value="{{old('seat_quantity')}}" placeholder="Jumlah Kursi" required>
							@if ($errors->has('seat_quantity'))
                 <div class="form-control-feedback">
                   <strong>{{ $errors->first('seat_quantity') }}</strong>
                 </div>
	            @endif
						</div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary" onclick="event.preventDefault();
                        document.getElementById('add-form').submit();">Simpan</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Insert --}}

{{-- Modal Delete --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Kursi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus kursi ini?
      </div>
      <div class="modal-footer">
        <form id="delete-form" action="{{url('admin\seat')}}" method="post">
          @csrf
          {{ method_field('delete') }}
          <input type="hidden" name="seat_id">
          <button type="submit" class="btn btn-danger" onclick="event.preventDefault();
                      document.getElementById('delete-form').submit();">Hapus</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Delete --}}

@if (session('status'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times</button>
        <strong>Selamat</strong>
        {{ session('status') }}
    </div>
@endif
<div class="row">
  <div class="col-md-12">
    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#insertModal">
      <i class="fa fa-plus"></i> Tambah Kursi
    </button>
  </div>
</div>
<hr>
<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th>Tipe Transportasi</th>
      <th>Nama Transportasi</th>
      <th>Kode Kursi</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @php($salim = 1)
    @foreach ($seats as $s)
      <tr>
        <td>{{$salim++}}</td>
        <td>{{$s->transportation->transportation_type->description}}</td>
        <td>{{$s->transportation->description}}</td>
        <td>{{$s->seat_code}}</td>
        <td>
          <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="deleteModal({{$s->id}},'{{$s->seat_code}}')"><i class="fa fa-close"></i></button>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
@endsection

@section('js')
<script type="text/javascript">
  function deleteModal(id, seat_code) {
    $('#deleteModal .modal-body').html('Apakah Anda yakin ingin menghapus kursi: '+seat_code+'?');
    $('#deleteModal [name=seat_id]').val(id);
  }
</script>
@endsection
