@extends('layouts.admin-layout')

@section('title')
  <i class="fa fa-transportation"></i> Tambah Transportasi
@endsection

@section('content')
  <form action="{{url('admin/transportasi')}}" method="post">
    {{ csrf_field() }}
    <div class="form-group {{ $errors->has('transportation_type') ? ' has-error has-danger' : '' }}">
      <label class="label">Tipe Transportasi</label>
      <div class="row">
        <div class="col-md-5">
          <select class="form-control" name="transportation_type">
            @foreach ($transportation_types as $tp)
              <option value="{{$tp->id}}">{{$tp->description}}</option>
            @endforeach
            @if ($errors->has('transportation_type'))
              <span class="invalid-feedback">
                  <strong>{{ $errors->first('transportation_type') }}</strong>
              </span>
            @endif
          </select>
        </div>
      </div>
    </div>
    <div class="form-group {{ $errors->has('code') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Kode Transportasi</label>

        <div class="row">
          <div class="col-md-6">
              <input type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code') }}" placeholder="Kode Transportasi" required>

              @if ($errors->has('code'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('code') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('description') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Nama Transportasi</label>

        <div class="row">
          <div class="col-md-6">
              <input type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" placeholder="Nama Transportasi" required>

              @if ($errors->has('description'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('description') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('seat_quantity') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Jumlah Kursi</label>

        <div class="row">
          <div class="col-md-3">
              <input type="number" class="form-control{{ $errors->has('seat_quantity') ? ' is-invalid' : '' }}" name="seat_quantity" value="{{ old('seat_quantity') }}" placeholder="Jumlah Kursi" required>

              @if ($errors->has('seat_quantity'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('seat_quantity') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>
    <hr>
    <button type="save" class="btn btn-success">Simpan</button>
  </form>
@endsection
