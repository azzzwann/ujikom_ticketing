@extends('layouts.admin-layout')
@section('title')
<i class="fa fa-train"></i> Transportasi
@endsection

@section('content')
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Transportasi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus transportasi ini?
      </div>
      <div class="modal-footer">
        <form id="delete-form" action="{{url('admin\transportasi')}}" method="post">
          {{ csrf_field() }}
          {{ method_field('delete') }}
          <input type="hidden" name="transportation_id">
          <button type="submit" class="btn btn-danger" onclick="event.preventDefault();
          document.getElementById('delete-form').submit();">Hapus</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
@if (session('status'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times</button>
  <strong>Selamat</strong>
  {{ session('status') }}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <a href="{{url('admin/transportasi/add')}}" class="btn btn-primary pull-right">
      <i class="fa fa-plus"></i> Tambah
    </a>
  </div>
</div>
<hr>
<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th>Tipe Transportasi</th>
      <th>Kode Transportasi</th>
      <th>Nama Transportasi</th>
      <th>Jumlah Kursi</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @php($azwan = 1)
    @foreach ($transportations as $t)
    <tr>
      <td>{{$azwan++}}</td>
      <td>{{$t->transportation_type->description}}</td>
      <td>{{$t->code}}</td>
      <td>{{$t->description}}</td>
      <td><a href="{{url('admin/transportasi/'.$t->id)}}" class="btn btn-default">{{$t->seats->count()}}</a></td>
      <td>
        <a href="{{url('admin/transportasi/'.$t->id.'/edit')}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="deleteModal({{$t->id}},'{{$t->description}}')"><i class="fa fa-close"></i></button>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection
@section('js')
<script type="text/javascript">
  function deleteModal(id, name) {
    $('#deleteModal .modal-body').html('Apakah Anda yakin ingin menghapus transportasi: '+name+'?');
    $('#deleteModal [name=transportation_id]').val(id);
  }
</script>
@endsection
