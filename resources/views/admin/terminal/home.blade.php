@extends('layouts.admin-layout')

@section('title')
<i class="fa fa-tag"></i> Terminal
@endsection

@section('content')
{{-- Modal Insert --}}
<div class="modal fade" id="insertModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Tambah Terminal</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="add-form" action="{{url('admin\terminal')}}" method="post">
          {{ csrf_field() }}
          <div class="form-group {{ $errors->has('transportation_type') ? ' has-error has-danger' : '' }}">
            <label class="form-control-label">Tipe Transportasi</label>
            <select class="form-control" name="transportation_type">
              @foreach ($transportation_types as $tp)
              <option value="{{$tp->id}}">{{$tp->description}}</option>
              @endforeach
              @if ($errors->has('transportation_type'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('transportation_type') }}</strong>
              </span>
              @endif
            </select>
          </div>
          <div class="form-group {{ $errors->has('name') ? ' has-error has-danger' : '' }}">
            <label class="form-control-label">Nama Terminal</label>
            <input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="Nama Terminal" required>
            @if ($errors->has('name'))
            <div class="form-control-feedback">
             <strong>{{ $errors->first('name') }}</strong>
           </div>
           @endif
         </div>
         <div class="form-group {{ $errors->has('city') ? ' has-error has-danger' : '' }}">
          <label class="form-control-label">Kota</label>
          <input type="text" name="city" class="form-control" value="{{old('city')}}" placeholder="Kota Terminal" required>
          @if ($errors->has('city'))
          <div class="form-control-feedback">
           <strong>{{ $errors->first('city') }}</strong>
         </div>
         @endif
       </div>
     </div>
     <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
  </div>
</div>
</div>
</div>
{{-- End Modal Insert --}}

{{-- Modal Edit --}}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Terminal</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <form id="edit-form" action="{{url('admin\terminal')}}" method="post">
          {{ csrf_field() }}
          {{ method_field('patch') }}
          <input type="hidden" name="terminal_id">
          <div class="form-group {{ $errors->has('edit_name') ? ' has-error has-danger' : '' }}">
            <label class="form-control-label">Nama Terminal</label>
            <input type="text" name="edit_name" class="form-control" value="{{old('edit_name')}}" placeholder="Nama Terminal" required>
            @if ($errors->has('edit_name'))
            <div class="form-control-feedback">
             <strong>{{ $errors->first('edit_name') }}</strong>
           </div>
           @endif
         </div>
         <div class="form-group {{ $errors->has('edit_city') ? ' has-error has-danger' : '' }}">
          <label class="form-control-label">Kota</label>
          <input type="text" name="edit_city" class="form-control" value="{{old('edit_city')}}" placeholder="Kota" required>
          @if ($errors->has('edit_city'))
          <div class="form-control-feedback">
           <strong>{{ $errors->first('edit_city') }}</strong>
         </div>
         @endif
       </div>
     </div>
     <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Edit</button>
    </form>
    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
  </div>
</div>
</div>
</div>
{{-- End Modal Edit --}}

{{-- Modal Delete --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Terminal</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus terminal ini?
      </div>
      <div class="modal-footer">
        <form id="delete-form" action="{{url('admin\terminal')}}" method="post">
          {{ csrf_field() }}
          {{ method_field('delete') }}
          <input type="hidden" name="terminal_id">
          <button type="submit" class="btn btn-danger">Hapus</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Delete --}}

@if (session('status'))
<div class="alert alert-success alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times</button>
  <strong>Selamat</strong>
  {{ session('status') }}
</div>
@endif
<div class="row">
  <div class="col-md-12">
    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#insertModal">
      <i class="fa fa-plus"></i> Tambah
    </button>
  </div>
</div>
<hr>
<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th>No.</th>
      <th>Tipe Transportasi</th>
      <th>Nama Terminal</th>
      <th>Kota</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @php($a = 1)
    @foreach ($terminals as $t)
    <tr>
      <td>{{$a++}}</td>
      <td>{{$t->transportation_type->description}}</td>
      <td>{{$t->name}}</td>
      <td>{{$t->city}}</td>
      <td>
        <a href="{{url('admin/terminal/'.$t->id.'/edit')}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
        <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="deleteModal({{$t->id}},'{{$t->name}}')"><i class="fa fa-close"></i></button>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection

@section('js')
<script type="text/javascript">
  function editModal(id, name, city) {
    $('#editModal [name=terminal_id]').val(id);
    $('#editModal [name=edit_name]').val(name);
    $('#editModal [name=edit_city]').val(city);
  }

  function deleteModal(id, name) {
    $('#deleteModal .modal-body').html('Apakah Anda yakin ingin menghapus terminal: '+name+'?');
    $('#deleteModal [name=terminal_id]').val(id);
  }
</script>
@endsection
