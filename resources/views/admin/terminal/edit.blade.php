@extends('layouts.admin-layout')

@section('title')
<i class="fa fa-train"></i> Edit Transportasi
@endsection

@section('content')
<form action="{{url('admin/terminal')}}" method="post">
  {{ csrf_field() }}
  {{ method_field('patch') }}
  <input type="hidden" name="terminal_id" value="{{$terminal->id}}">
  <div class="form-group {{ $errors->has('transportation_type') ? ' has-error has-danger' : '' }}">
    <label class="label">Tipe Transportasi</label>
    <div class="row">
      <div class="col-md-5">
        <select class="form-control" name="transportation_type">
          @foreach ($transportation_types as $tp)
          @if ($tp->id == $terminal->transportation_type_id)
          <option value="{{$tp->id}}" selected>{{$tp->description}}</option>
          @else
          <option value="{{$tp->id}}">{{$tp->description}}</option>
          @endif
          @endforeach
          @if ($errors->has('name'))
          <span class="invalid-feedback">
            <strong>{{ $errors->first('transportation_type') }}</strong>
          </span>
          @endif
        </select>
      </div>
    </div>
  </div>
  <div class="form-group {{ $errors->has('edit_name') ? ' has-error has-danger' : '' }}">
    <label class="form-control-label">Nama Terminal</label>
    <input type="text" name="edit_name" class="form-control" value="{{$terminal->name}}" placeholder="Nama Terminal" required>
    @if ($errors->has('edit_name'))
    <div class="form-control-feedback">
     <strong>{{ $errors->first('edit_name') }}</strong>
   </div>
   @endif
 </div>
 <div class="form-group {{ $errors->has('edit_city') ? ' has-error has-danger' : '' }}">
  <label class="form-control-label">Kota</label>
  <input type="text" name="edit_city" class="form-control" value="{{$terminal->city}}" placeholder="Kota" required>
  @if ($errors->has('edit_city'))
  <div class="form-control-feedback">
   <strong>{{ $errors->first('edit_city') }}</strong>
 </div>
 @endif
</div>

<hr>
<button type="save" class="btn btn-success">Simpan</button>
</form>
@endsection
