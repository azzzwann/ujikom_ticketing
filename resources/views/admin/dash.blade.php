@extends('layouts.admin-layout')
@section('content')
<section class="content-header">
	<h1>
		Dashboard
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
	</ol>
</section>
<section class="content container-fluid">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">CPU Traffic</span>
					<span class="info-box-number">90<small>%</small></span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">Likes</span>
					<span class="info-box-number">41,410</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->

		<!-- fix for small devices only -->
		<div class="clearfix visible-sm-block"></div>

		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">Sales</span>
					<span class="info-box-number">760</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="info-box">
				<span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">New Members</span>
					<span class="info-box-number">2,000</span>
				</div>
				<!-- /.info-box-content -->
			</div>
			<!-- /.info-box -->
		</div>
		<!-- /.col -->
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Simple Full Width Table</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<table class="table table-hover">
						<tr>
							<th style="width: 10px">#</th>
							<th>Task</th>
							<th>Progress</th>
							<th style="width: 40px">Label</th>
						</tr>
						<tr>
							<td>1.</td>
							<td>Update software</td>
							<td>
								<div class="progress progress-xs">
									<div class="progress-bar progress-bar-danger" style="width: 55%"></div>
								</div>
							</td>
							<td><span class="badge bg-red">55%</span></td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Clean database</td>
							<td>
								<div class="progress progress-xs">
									<div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
								</div>
							</td>
							<td><span class="badge bg-yellow">70%</span></td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Cron job running</td>
							<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-primary" style="width: 30%"></div>
								</div>
							</td>
							<td><span class="badge bg-light-blue">30%</span></td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Fix and squish bugs</td>
							<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-success" style="width: 90%"></div>
								</div>
							</td>
							<td><span class="badge bg-green">90%</span></td>
						</tr>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
		<div class="col-md-6">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Simple Full Width Table</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<table class="table table-hover">
						<tr>
							<th style="width: 10px">#</th>
							<th>Task</th>
							<th>Progress</th>
							<th style="width: 40px">Label</th>
						</tr>
						<tr>
							<td>1.</td>
							<td>Update software</td>
							<td>
								<div class="progress progress-xs">
									<div class="progress-bar progress-bar-danger" style="width: 55%"></div>
								</div>
							</td>
							<td><span class="badge bg-red">55%</span></td>
						</tr>
						<tr>
							<td>2.</td>
							<td>Clean database</td>
							<td>
								<div class="progress progress-xs">
									<div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
								</div>
							</td>
							<td><span class="badge bg-yellow">70%</span></td>
						</tr>
						<tr>
							<td>3.</td>
							<td>Cron job running</td>
							<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-primary" style="width: 30%"></div>
								</div>
							</td>
							<td><span class="badge bg-light-blue">30%</span></td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Fix and squish bugs</td>
							<td>
								<div class="progress progress-xs progress-striped active">
									<div class="progress-bar progress-bar-success" style="width: 90%"></div>
								</div>
							</td>
							<td><span class="badge bg-green">90%</span></td>
						</tr>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Responsive Hover Table</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tr>
							<th>ID</th>
							<th>User</th>
							<th>Date</th>
							<th>Status</th>
							<th>Reason</th>
						</tr>
						<tr>
							<td>183</td>
							<td>John Doe</td>
							<td>11-7-2014</td>
							<td><span class="label label-success">Approved</span></td>
							<td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
						</tr>
						<tr>
							<td>219</td>
							<td>Alexander Pierce</td>
							<td>11-7-2014</td>
							<td><span class="label label-warning">Pending</span></td>
							<td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
						</tr>
						<tr>
							<td>657</td>
							<td>Bob Doe</td>
							<td>11-7-2014</td>
							<td><span class="label label-primary">Approved</span></td>
							<td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
						</tr>
						<tr>
							<td>175</td>
							<td>Mike Doe</td>
							<td>11-7-2014</td>
							<td><span class="label label-danger">Denied</span></td>
							<td>Bacon ipsum dolor sit amet salami venison chicken flank fatback doner.</td>
						</tr>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</section>
@endsection

