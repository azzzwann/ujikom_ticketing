@extends('layouts.admin-layout')

@section('title')
  <i class="fa fa-road"></i> Rute
@endsection

@section('content')
{{-- Modal Delete --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Hapus Transportasi</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus rute ini?
      </div>
      <div class="modal-footer">
        <form id="delete-form" action="{{url('admin\rute')}}" method="post">
          {{ csrf_field() }}
          {{ method_field('delete') }}
          <input type="hidden" name="rute_id">
          <button type="submit" class="btn btn-danger" onclick="event.preventDefault();
                      document.getElementById('delete-form').submit();">Hapus</button>
        </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
{{-- End Modal Delete --}}
    @if (session('status'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">&times</button>
            <strong>Selamat</strong>
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
      <div class="col-md-12">
        <a href="{{url('admin/rute/add')}}" class="btn btn-primary pull-right">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
    </div>
    <hr>
    <table class="table table-striped table-bordered table-hover">
      <thead>
        <tr>
          <th>No.</th>
          <th>Tipe Transportasi</th>
          <th>Kode Transportasi</th>
          <th>Nama Transportasi</th>
          <th>Jam Keberangkatan</th>
          <th>Berangkat Dari</th>
          <th>Tujuan</th>
          <th>Harga (Rp)</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        @php($salim = 1)
        @foreach ($rutes as $rute)
          <tr>
            <td>{{$salim++}}</td>
            <td>{{$rute->transportation->transportation_type->description}}</td>
            <td>{{$rute->transportation->code}}</td>
            <td>{{$rute->transportation->description}}</td>
            <td>{{$rute->depart_at}}</td>
            <td>{{$rute->origin->name}}</td>
            <td>{{$rute->destination->name}}</td>
            <td>{{number_format($rute->price)}}</td>
            <td>
              <a href="{{url('admin/rute/'.$rute->id.'/edit')}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
              <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal" onclick="deleteModal({{$rute->id}})"><i class="fa fa-close"></i></button>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
@endsection
@section('js')
<script type="text/javascript">
function deleteModal(id) {
  $('#deleteModal [name=rute_id]').val(id);
}
</script>
@endsection
