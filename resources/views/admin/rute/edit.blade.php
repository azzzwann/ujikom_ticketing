@extends('layouts.admin-layout')

@section('title')
  <i class="fa fa-road"></i> Edit Rute
@endsection

@section('content')
  <form action="{{url('admin/rute')}}" method="post">
    {{ csrf_field() }}
    {{ method_field('patch') }}
    <input type="hidden" name="rute_id" value="{{$rute->id}}">
    <div class="form-group {{ $errors->has('transportation') ? ' has-error has-danger' : '' }}">
      <label>Transportasi</label>
      <div class="row">
        <div class="col-md-5">
          <select class="form-control" name="transportation">
            @foreach ($transportations as $tp)
              @if ($rute->transportation->id)
                <option value="{{$tp->id}}" selected>{{$tp->description}}</option>
              @else
                <option value="{{$tp->id}}">{{$tp->description}}</option>
              @endif
            @endforeach
            @if ($errors->has('transportation'))
              <span class="invalid-feedback">
                  <strong>{{ $errors->first('transportation') }}</strong>
              </span>
            @endif
          </select>
        </div>
      </div>
    </div>
    <div class="form-group {{ $errors->has('depart_at') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Waktu Keberangkatan</label>

        <div class="row">
          <div class="col-md-2">
              <select class="form-control" name="depart_at">
                @for ($i=1; $i <= 24; $i++)
                  @if ($i == 24)
                    <option>{{($i > 9?$i:"0".$i).":00"}}</option>
                  @else
                    <option>{{($i > 9?$i:"0".$i).":00"}}</option>
                    <option>{{($i > 9?$i:"0".$i).":30"}}</option>
                  @endif
                @endfor
              </select>

              @if ($errors->has('depart_at'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('depart_at') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('rute_from') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Berangkat dari</label>

        <div class="row">
          <div class="col-md-6">
              <select class="form-control" name="rute_from">
                @foreach ($terminals as $t)
                  @if ($t->id == $rute->rute_from)
                    <option value="{{$t->id}}" selected>{{$t->name}}</option>
                  @else
                    <option value="{{$t->id}}">{{$t->name}}</option>
                  @endif
                @endforeach
              </select>
              @if ($errors->has('rute_from'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('rute_from') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('rute_to') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Tujuan</label>

        <div class="row">
          <div class="col-md-6">
              <select class="form-control" name="rute_to">
                @foreach ($terminals as $t)
                  @if ($t->id == $rute->rute_to)
                    <option value="{{$t->id}}" selected>{{$t->name}}</option>
                  @else
                    <option value="{{$t->id}}">{{$t->name}}</option>
                  @endif
                @endforeach
              </select>
              @if ($errors->has('rute_to'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('rute_to') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>

    <div class="form-group {{ $errors->has('price') ? ' has-error has-danger' : '' }}">
        <label class="col-form-label">Harga</label>

        <div class="row">
          <div class="col-md-3">
              <input type="number" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{ count($errors) ? old('price') : $rute->price }}" placeholder="Harga" required>

              @if ($errors->has('price'))
                  <span class="invalid-feedback">
                      <strong>{{ $errors->first('price') }}</strong>
                  </span>
              @endif
          </div>
        </div>
    </div>
    <hr>
    <button type="save" class="btn btn-success">Simpan</button>
  </form>
@endsection
