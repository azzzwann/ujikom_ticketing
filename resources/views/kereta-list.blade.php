@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header"><i class="fa fa-train"></i> Daftar Kereta Api</div>

                <div class="card-body">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Transportasi</th>
                        <th>Berangkat</th>
                        <th>Harga</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($rutes as $rute)
                        <tr>
                          <td>{{$rute->transportation->description}}</td>
                          <td>{{$rute->depart_at}}</td>
                          <td>Rp {{number_format($rute->price)}}</td>
                          <td>
                            @if (count($rute->reservations->where('reservation_date', $date)))
                              @php
                                $seat_qty = $rute->transportation->seats->count();
                                $booked = count($rute->reservations->where('reservation_date', $date));
                                $stock = $seat_qty - $booked;
                              @endphp
                              @if ($stock<1)
                                <b>Habis</b>
                              @else
                                @auth
                                  <form action="{{url('booking?date='.$date.'&rute='.$rute->id.'&adult='.$adult.'&infant='.$infant.'')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="date" value="{{$date}}">
                                    <input type="hidden" name="rute" value="{{$rute->id}}">
                                    <input type="hidden" name="adult" value="{{$adult}}">
                                    <input type="hidden" name="infant" value="{{$infant}}">
                                    <button type="submit" class="btn btn-success">Booking</button>
                                  </form>
                                @endauth
                                @guest
                                  <form action="{{url('booking')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="date" value="{{$date}}">
                                    <input type="hidden" name="rute" value="{{$rute->id}}">
                                    <input type="hidden" name="adult" value="{{$adult}}">
                                    <input type="hidden" name="infant" value="{{$infant}}">
                                    <button type="submit" class="btn btn-success">Booking</button>
                                  </form>
                                @endguest
                              @endif
                            @else
                              @auth
                                <form action="{{url('booking?date='.$date.'&rute='.$rute->id.'&adult='.$adult.'&infant='.$infant.'')}}" method="post">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="date" value="{{$date}}">
                                  <input type="hidden" name="rute" value="{{$rute->id}}">
                                  <input type="hidden" name="adult" value="{{$adult}}">
                                  <input type="hidden" name="infant" value="{{$infant}}">
                                  <button type="submit" class="btn btn-success">Booking</button>
                                </form>
                              @endauth
                              @guest
                                <form action="{{url('booking')}}" method="post">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="date" value="{{$date}}">
                                  <input type="hidden" name="rute" value="{{$rute->id}}">
                                  <input type="hidden" name="adult" value="{{$adult}}">
                                  <input type="hidden" name="infant" value="{{$infant}}">
                                  <button type="submit" class="btn btn-success">Booking</button>
                                </form>
                              @endguest
                            @endif
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
