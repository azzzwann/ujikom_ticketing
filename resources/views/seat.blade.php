@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header"><i class="fa fa-train"></i> Kereta Api</div>

                <div class="card-body">
                  <div class="row">
                    <div class="col-md-4">
                      <b>Stasiun Asal</b>: {{$rute->origin->name}}
                    </div>
                    <div class="col-md-4">
                      <b>Stasiun Tujuan</b>: {{$rute->destination->name}}
                    </div>
                    <div class="col-md-4">
                      <b>Tanggal Keberangkatan</b>: {{$date}}. Jam {{$rute->depart_at}}
                    </div>
                  </div>
                  <hr>
                  @for ($i=0; $i < count($name); $i++)
                    <div class="row">
                      <div class="col-md-4">
                        <b>Nama Penumpang</b> : {{$name[$i]}}
                      </div>
                      <div class="col-md-4">
                        <b>Jenis Kelamin</b> : {{$gender[$i]}}
                      </div>
                      <div class="col-md-4">
                        <b>Nomor Kursi</b>:
                        {{$seats[$i]->seat_code}}
                      </div>
                    </div>
                  @endfor
                  <hr>
                  <div class="row">
                    <div class="col-md-4">
                      <b>Nama Pemesan</b> : {{Auth::user()->fullname}}
                    </div>
                    <div class="col-md-4">
                      <b>No. Hp</b> : {{$phone}}
                    </div>
                    <div class="col-md-4">
                      <b>Alamat</b> : {{$address}}
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-md-12">
                      <form action="{{url('booking/confirm-booking')}}" method="post">
                        {{ csrf_field() }}
                        @for ($i=0; $i < count($name); $i++)
                          <input type="hidden" name="name[{{$i}}]" value="{{$name[$i]}}">
                          <input type="hidden" name="gender[{{$i}}]" value="{{$gender[$i]}}">
                          <input type="hidden" name="seat[{{$i}}]" value="{{$rute->transportation->seats[$i]}}">
                        @endfor
                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        <input type="hidden" name="rute" value="{{$rute->id}}">
                        <input type="hidden" name="phone" value="{{$phone}}">
                        <input type="hidden" name="address" value="{{$address}}">
                        <input type="hidden" name="date" value="{{$date}}">
                        <button type="submit" class="btn btn-success pull-right">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
