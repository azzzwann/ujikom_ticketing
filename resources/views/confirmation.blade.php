@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="card">
                <div class="card-header"><i class="fa fa-train"></i> Kereta Api</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h4 style="text-align: center;">Data Booking</h4>
                    <div class="col-md-12" style="padding: 0px; margin-top: 20px">
                      <h3 style="font-size: 13pt; margin: 0px; padding: 0px">Data Penumpang & Data Pemesan</h3>
                      <div class="row">
                        <br>
                        <div class="col-md-6" style="border-right: 1px solid #D0D9D6; font-size: 13px; max-height:">
                          <div class="col-md-12">
                            @for ($i=0; $i < count($name); $i++)
                              <div class="row">
                                <div class="col-md-4" style="padding: 0px">
                                    Nama
                                </div>
                                <div class="col-md-8" style="padding: 0px">
                                    : {{$name[$i]}}
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-4" style="padding: 0px; padding-bottom: 10px">
                                    Jenis Kelamin
                                </div>
                                <div class="col-md-8" style="padding: 0px; padding-bottom: 10px">
                                    : {{$gender[$i]}} <br>
                                </div>
                              </div>
                            @endfor
                          </div>
                        </div>

                        <div class="col-md-6" style=" font-size: 14px">
                            <div class="col-md-4" style="padding: 0px">
                                Nama
                            </div>
                            <div class="col-md-8" style="padding: 0px">
                                : {{Auth::user()->fullname}}
                            </div>
                            <div class="col-md-4" style="padding: 0px">
                                No. HP
                            </div>
                            <div class="col-md-8" style="padding: 0px">
                                : {{$phone}}
                            </div>
                            <div class="col-md-4" style="padding: 0px">
                                Alamat
                            </div>
                            <div class="col-md-8" style="padding: 0px">
                                : {{$address}}
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12" style="padding: 0px">
                        <h3 style="font-size: 13pt; margin: 0px; padding: 0px">Tiket Pesanan</h3>
                        <br>
                        <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-3" style="padding: 0px">
                                Dari
                            </div>
                            <div class="col-md-9" style="padding: 0px">
                                : {{$rute->origin->name}}
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3" style="padding: 0px">
                                Tujuan
                            </div>
                            <div class="col-md-9" style="padding: 0px">
                                : {{$rute->destination->name}}
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3" style="padding: 0px">
                                Tanggal Berangkat
                            </div>
                            <div class="col-md-9" style="padding: 0px">
                                : {{$date}}
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3" style="padding: 0px">
                                Jumlah Penumpang
                            </div>
                            <div class="col-md-9" style="padding: 0px">
                                : {{count($name)}}
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-3" style="padding: 0px">
                                Total
                            </div>
                            <div class="col-md-9" style="padding: 0px">
                                : Rp {{number_format($rute->price)}}
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <form action="{{url('booking/make-booking')}}" method="post">
                          {{ csrf_field() }}
                          @for ($i=0; $i < count($name); $i++)
                            <input type="hidden" name="name[{{$i}}]" value="{{$name[$i]}}">
                            <input type="hidden" name="gender[{{$i}}]" value="{{$gender[$i]}}">
                            <input type="hidden" name="seat[{{$i}}]" value="{{$rute->transportation->seats[$i]->id}}">
                          @endfor
                          <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                          <input type="hidden" name="rute" value="{{$rute->id}}">
                          <input type="hidden" name="phone" value="{{$phone}}">
                          <input type="hidden" name="address" value="{{$address}}">
                          <input type="hidden" name="date" value="{{$date}}">
                          <button type="submit" class="btn btn-primary pull-right">Konfirmasi</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
