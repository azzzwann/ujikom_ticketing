<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportationType extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function transportations()
    {
        return $this->hasMany(Transportation::class);
    }
}
