<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Terminal extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function transportation_type()
    {
        return $this->belongsTo(TransportationType::class);
    }
    
    public function rute_from()
    {
        return $this->hasMany(Rute::class,'rute_from','id');
    }

    public function rute_to()
    {
        return $this->hasMany(Rute::class,'rute_to','id');
    }
}
