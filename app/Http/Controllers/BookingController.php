<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Rute;
use App\Seat;
use App\Customer;
use App\Reservation;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function index(Request $r)
    {
        if (Auth::user()) {
          $data = $r->all();
          return view('form', $data);
        }else {
          Session::put('book',$r->all());
          return redirect(route('login'));
        }
    }

    public function seat(Request $r)
    {
        $data = $r->all();
        $data['rute'] = Rute::find($r->rute);
        $data['reservased'] = Reservation::select('seat_id')
                                          ->where([
                                                  ['rute_id',$data['rute']->id],
                                                  ['reservation_date',$data['date']]
                                                ])
                                          ->get();
        $reservased = array();
        foreach ($data['reservased'] as $res) {
          array_push($reservased, $res->seat_id);
        }

        $data['seats'] = Seat::where('transportation_id',$data['rute']->transportation->id)
                               ->whereNotIn('id', $reservased)
                               ->get();

        // dd($data);
        return view('seat',$data);
    }

    public function confirm(Request $r)
    {
        $data = $r->all();
        $data['rute'] = Rute::find($r->rute);
        return view('confirmation', $data);
    }

    public function make(Request $r)
    {
        $data = $r->all();
        $data['rute'] = Rute::find($r->rute);

        $reservation_code = $data['rute']->id."_".$data['user_id']."_".date('Ymdhis');
        for ($i=0; $i < count($data['name']) ; $i++) {
          $gender = $data['gender'][$i] == "Laki-laki"?'L':'P';
          $customer = Customer::create([
            'name'    => $data['name'][$i],
            'address' => $data['address'],
            'phone'   => $data['phone'],
            'gender'  => $gender
          ]);

          Reservation::create([
            'user_id'           => $data['user_id'],
            'customer_id'       => $customer->id,
            'rute_id'           => $data['rute']->id,
            'seat_id'           => $data['seat'][$i],
            'reservation_code'  => $reservation_code,
            'reservation_at'    => date('h:i:s'),
            'reservation_date'  => date('Y-m-d'),
            'depart_at'         => $data['rute']->depart_at,
            'price'             => $data['rute']->price
          ]);
        }

        Session::forget('book');
        return redirect(route('home'));
    }
}
