<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use App\Rute;
use App\Terminal;
use App\Transportation;
use Illuminate\Http\Request;
use App\Http\Requests\RuteRequest;
use App\Http\Controllers\Controller;

class RuteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request,$next){
            if(Auth::user()->level != 'admin'){
                return redirect('home');
            }
            return $next($request);
        });
    }
    public function index()
    {
        $rutes = Rute::all();
        return view('admin.rute.home', compact('rutes'));
    }

    public function add()
    {
        $transportations = Transportation::all();
        $terminals = Terminal::all();
        return view('admin.rute.add', compact('transportations','terminals'));
    }

    public function edit(Rute $rute)
    {
        $transportations = Transportation::all();
        $terminals = Terminal::all();
        return view('admin.rute.edit', compact('transportations','rute','terminals'));
    }

    public function store(RuteRequest $r)
    {
        Rute::create([
          'transportation_id' => $r->transportation,
          'depart_at'         => $r->depart_at,
          'rute_from'         => $r->rute_from,
          'rute_to'           => $r->rute_to,
          'price'             => $r->price
        ]);

        Session::flash('status', 'Anda berhasil menambah rute');
        return redirect('admin/rute');
    }

    public function update(RuteRequest $r)
    {
        Rute::find($r->rute_id)->update([
          'transportation_id' => $r->transportation,
          'depart_at'         => $r->depart_at,
          'rute_from'         => $r->rute_from,
          'rute_to'           => $r->rute_to,
          'price'             => $r->price
        ]);

        Session::flash('status', 'Anda berhasil mengedit rute');
        return redirect('admin/rute');
    }

    public function delete(Request $r)
    {
        Rute::find($r->rute_id)->delete();

        Session::flash('status', 'Anda berhasil menghapus rute');
        return redirect('admin/rute');
    }
}
