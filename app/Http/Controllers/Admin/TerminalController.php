<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use App\Terminal;
use App\Transportation;
use App\TransportationType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TerminalRequest;

class TerminalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request,$next){
            if(Auth::user()->level != 'admin'){
                return redirect('home');
            }
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $terminals = Terminal::all();
        $transportation_types = TransportationType::all();
        // dd($terminals);
        return view('admin.terminal.home', compact('terminals','transportation_types'));
    }

    public function edit(Terminal $terminal)
    {
        $transportation_types = TransportationType::all();
        return view('admin.terminal.edit', compact('terminal', 'transportation_types'));
    }
    public function store(Request $r)
    {
        Terminal::create([
          'transportation_type_id' => intval($r->transportation_type),
          'name' => $r->name,
          'city' => $r->city
        ]);

        Session::flash('status', 'Anda berhasil menambah terminal');
        return redirect(url()->previous());
    }

    public function update(TerminalRequest $r)
    {
        Terminal::find($r->terminal_id)->update([
          'transportation_type_id' => $r->transportation_type,
          'name' => $r->edit_name,
          'city' => $r->edit_city
        ]);

        Session::flash('status', 'Anda berhasil mengedit terminal');
        return redirect(url('admin/terminal'));
    }

    public function delete(Request $r)
    {
        Terminal::find($r->terminal_id)->delete();

        Session::flash('status', 'Anda berhasil menghapus tipe terminal');
        return redirect(url()->previous());
    }
}
