<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Session;
use App\Seat;
use App\Transportation;
use App\TransportationType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TransportationRequest;

class TransportasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request,$next){
            if(Auth::user()->level != 'admin'){
                return redirect('home');
            }
            return $next($request);
        });
    }

    public function index()
    {
        $transportations = Transportation::all();
        return view('admin.transportasi.home', compact('transportations'));
    }

    public function show(Transportation $transportation)
    {
        $seats = $transportation->seats;
        return view('admin.transportasi.show', compact('transportation','seats'));
    }

    public function add()
    {
        $transportation_types = TransportationType::all();
        return view('admin.transportasi.add', compact('transportation_types'));
    }

    public function edit(Transportation $transportation)
    {
        $transportation_types = TransportationType::all();
        return view('admin.transportasi.edit', compact('transportation', 'transportation_types'));
    }

    public function store(TransportationRequest $r)
    {
        $transportation = Transportation::create([
          'transportation_type_id' => $r->transportation_type,
          'code'                   => $r->code,
          'description'            => $r->description
        ]);

        for ($i=1; $i <= $r->seat_quantity ; $i++) {
          Seat::create([
            'transportation_id' => $transportation->id,
            'seat_code'         => '999'.$transportation->id.'SEAT_'.$i
          ]);
        }

        Session::flash('status', 'Anda berhasil menambah transportasi');
        return redirect(url('admin/transportasi'));
    }

    public function update(TransportationRequest $r)
    {
        Transportation::find($r->transportation_id)->update([
          'transportation_type_id' => $r->transportation_type,
          'description'            => $r->description
        ]);

        Session::flash('status', 'Anda berhasil mengedit transportasi');
        return redirect(url('admin/transportasi'));
    }

    public function delete(Request $r)
    {
        Transportation::find($r->transportation_id)->delete();

        Session::flash('status', 'Anda berhasil menghapus transportasi');
        return redirect(url('admin/transportasi'));
    }
}
