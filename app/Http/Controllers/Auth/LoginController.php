<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected function authenticated(Request $request, $user)
    {
        if($user->level == 'admin'){
            return redirect('/admin/transportasi');
        }
        else{
            return redirect('home');
        }
    }
    
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
