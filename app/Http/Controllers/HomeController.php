<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::has('book')) {
          $data = Session::get('book');
          return redirect('booking?date='.$data['date'].'&rute='.$data['rute'].'&adult='.$data['adult'].'&infant='.$data['infant'].'');
        }
        return view('home');
    }
}
