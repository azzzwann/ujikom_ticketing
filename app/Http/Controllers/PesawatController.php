<?php

namespace App\Http\Controllers;

use App\Rute;
use App\Terminal;
use App\Transportation;
use Illuminate\Http\Request;

class PesawatController extends Controller
{
	public function index()
	{
		$terminals = Terminal::where('transportation_type_id', 2)->get();
		return view('pesawat', compact('terminals'));
	}

	public function search(Request $r)
	{

		$rutes = Rute::where([
			['rute_from', $r->rute_from],
			['rute_to', $r->rute_to]
		])
		->get();

		$data = [
			'date'  => $r->date,
			'rutes' => $rutes,
			'adult' => $r->adult,
			'infant'=> $r->infant
		];
		return view('pesawat-list', $data);
	}
}
