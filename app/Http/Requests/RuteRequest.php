<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RuteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
          return [
            'rute_id'        => 'required',
            'transportation' => 'required|integer',
            'depart_at'      => 'required|date_format:H:i',
            'rute_from'      => 'required',
            'rute_to'        => 'required',
            'price'          => 'required|numeric'
          ];
        }else {
          return [
            'transportation' => 'required|integer',
            'depart_at'      => 'required|date_format:H:i',
            'rute_from'      => 'required',
            'rute_to'        => 'required',
            'price'          => 'required|numeric'
          ];
        }
    }
}
