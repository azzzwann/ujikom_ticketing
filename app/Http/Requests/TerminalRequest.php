<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TerminalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
          return [
              'terminal_id'  => 'required|integer',
              'edit_name'         => 'required|string',
              'edit_city'         => 'required|string'
          ];
        }else {
          return [
              'terminal_id'  => 'required|integer',
              'edit_name'         => 'required|string',
              'edit_city'         => 'required|string'
          ];
        }
    }
}
