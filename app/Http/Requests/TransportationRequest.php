<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransportationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH') {
          return [
              'transportation_id'    => 'required|integer',
              'transportation_type' => 'required|integer',
              'code'                => 'required|string',
              'description'         => 'required|string'
          ];
        }else {
          return [
              'transportation_type' => 'required|integer',
              'code'                => 'required|string|min:5|unique:transportations',
              'description'         => 'required|string',
              'seat_quantity'       => 'required|integer'
          ];
        }
    }
}
