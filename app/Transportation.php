<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Transportation extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function transportation_type()
    {
        return $this->belongsTo(TransportationType::class);
    }

    public function seats()
    {
        return $this->hasMany(Seat::class);
    }

    public function rutes()
    {
        return $this->hasMany(Rute::class);
    }
}
